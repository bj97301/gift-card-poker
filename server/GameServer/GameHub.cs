﻿using System.Linq;
using Microsoft.AspNet.SignalR;
using GCPoker.Model;

namespace GameServer
{
    public class GameHub : Hub
    {
        IGameController _controller;

        public GameHub(IGameController controller)
        {
            _controller = controller;
        }

        public void InitializeClientGame(string playerName)
        {
            // Return current user data and all available games
            if (_controller.UserExists(playerName))
            {
                var player = _controller.GetPlayerByName(playerName);
                _controller.UpdatePlayerId(playerName, this.Context.ConnectionId);

                //TODO: deal with re-joining games

                ReturnGameOptions();                
            }
            else
            {
                _controller.AddPlayer(playerName, this.Context.ConnectionId);
                ReturnGameOptions();
            }
        }

        private void ReturnGameOptions()
        {
            Clients.Caller.gameInitialized(_controller.GetGame());
        }

        public void JoinGame(int gameRoomId)
        {
            // getGameRoomById(gameId)
            var room = _controller.GetGameRoom(gameRoomId);            
            
            // Determine if game is still available
            var currentPlayer = _controller.GetPlayerById(this.Context.ConnectionId);
            if (room.JoinGame(currentPlayer))
            {
                // Notify all clients that the player joined
                Clients.All.playerJoinedGame(currentPlayer, room);

                if (room.IsFull())
                {
                    foreach (var player in room.Players)
                    {
                        // Get players hand
                        var hand = room.GetHand(player.ConnectionId);
                        Clients.Client(player.ConnectionId).gameStarted(room, hand);
                    }

                    // Allow first player who joined to begin
                    Clients.Client(room.Players[0].ConnectionId).beginTurn(room);
                }
            }
            else
            {
                // Send notification to all clients that the game room is no longer available
                Clients.Caller.gameRoomNotAvailable(gameRoomId);
            }
        }

        public void Discard(int[] cards, int gameRoomId)
        {
            var room = _controller.GetGameRoom(gameRoomId);
            var drawnCards = room.DiscardAndDrawNew(cards, Context.ConnectionId);

            // If it wasn't the last user, call 'beginTurn' on next user.
            // Else then call 'gameEnded'
            if (room.GameStatus.Equals(GameStatus.Ended))
            {
                // for each player, return the *other* players' hands
                foreach (var connectionId in _controller.GetAllPlayers().Select(p => p.ConnectionId))
                {
                    var otherHands = _controller.GetAllOtherPlayersHands(connectionId, room.Id);
                    Clients.Client(connectionId).gameEnded(room, otherHands);                    
                }                
            }
            else
            {
                // Allow the next user to go
                Clients.Client(room.NextPlayerAfter(Context.ConnectionId).ConnectionId).beginTurn(room);
            }

            Clients.Caller.cardsDrawn(drawnCards, room);
        }
    }
}