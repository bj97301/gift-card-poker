﻿using Microsoft.Owin;
using Microsoft.Owin.Cors;
using GCPoker.Model;
using Microsoft.AspNet.SignalR;
using Ninject;
using Owin;

[assembly: OwinStartup(typeof(GameServer.Startup))]

namespace GameServer
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var config = new HubConfiguration();

            // Ninject stuff here (Unity is not "easily" compatible with SignalR)
            var kernel = new StandardKernel();
            var resolver = new NinjectSignalRDependencyResolver(kernel);
            kernel.Bind<ICardRepository>().To<InMemoryCardRepository>();
            kernel.Bind<IPlayerRepository>().To<InMemoryPlayerRepository>();
            kernel.Bind<IGame>().To<Game>();
            kernel.Bind<IGameController>().To<GameController>().InSingletonScope();
            config.Resolver = resolver;
            config.EnableDetailedErrors = true;

            app.MapSignalR(config);

            app.UseCors(CorsOptions.AllowAll);
        }
    }
}
