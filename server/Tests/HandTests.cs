﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using GCPoker.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using NUnit.Framework.Constraints;

namespace Tests
{
    [TestFixture]
    public class HandTests
    {
        private Hand _hand;
        private List<Card> _deck;

        [SetUp]
        public void Setup()
        {
            _deck = new List<Card>();
            foreach (var card in new Dealer().ShuffleCards(new InMemoryCardRepository().GetFullDeck()))
            {
                _deck.Add(card);
            }
        }

        [Test]
        public void HandWithTwoSameDenoms_ShouldBeOnePair()
        {
            GivenAHand()
                .WithNofAKindCards(2, DenomCode.Eighty, BrandCode.Amazon, BrandCode.ITunes);
            ThenHandShouldBe(HandTypes.OnePair)
                .AndHandShouldNotBe(HandTypes.ThreeOfAKind)
                .AndHandShouldNotBe(HandTypes.FourOfAKind);
        }

        [Test]
        public void HandWithThreeSameDenoms_ShouldBeThreeOfAKind()
        {
            GivenAHand().WithNofAKindCards(3, DenomCode.Eighty, BrandCode.Amazon, BrandCode.ITunes, BrandCode.Starbucks);
            ThenHandShouldBe(HandTypes.ThreeOfAKind)
                .AndHandShouldNotBe(HandTypes.FourOfAKind);
        }

        [Test]
        public void HandWithFourSameDenoms_ShouldBeFourOfAKind()
        {
            GivenAHand().WithNofAKindCards(4, DenomCode.Eighty, BrandCode.Amazon, BrandCode.ITunes, BrandCode.Starbucks, BrandCode.Target);
            ThenHandShouldBe(HandTypes.FourOfAKind);
        }

        [Test]
        public void HandWithTwoPairsTwoSameDenoms_ShouldBeTwoPair()
        {
            GivenAHand()
                .WithNofAKindCards(2, DenomCode.Eighty, BrandCode.Amazon, BrandCode.ITunes)
                .WithNofAKindCards(2, DenomCode.OneHundred, BrandCode.Starbucks, BrandCode.Target);
            ThenHandShouldBe(HandTypes.TwoPair);
        }

        [Test]
        public void HandWithTwoPairsTwoThreeSameDenoms_ShouldBeFullHouse()
        {
            GivenAHand()
                .WithNofAKindCards(2, DenomCode.Eighty, BrandCode.Amazon, BrandCode.ITunes)
                .WithNofAKindCards(3, DenomCode.OneHundred, BrandCode.Starbucks, BrandCode.Target, BrandCode.ITunes);
            ThenHandShouldBe(HandTypes.FullHouse);
        }

        [Test]
        public void HandWithDifferingBrandSequence_ShouldBeStraight()
        {
            GivenAHand().WithCards(new CardOption[]
            {
                new CardOption { BrandCode = BrandCode.Amazon, DenomCode = DenomCode.Twenty },
                new CardOption { BrandCode = BrandCode.ITunes, DenomCode = DenomCode.Thirty },
                new CardOption { BrandCode = BrandCode.Starbucks, DenomCode = DenomCode.Forty },
                new CardOption { BrandCode = BrandCode.Target, DenomCode = DenomCode.Fifty },
                new CardOption { BrandCode = BrandCode.Amazon, DenomCode = DenomCode.Sixty }
            });
            ThenHandShouldBe(HandTypes.Straight);
        }

        [Test]
        public void HandWithSameBrandNotSequence_ShouldBeFlush()
        {
            GivenAHand().WithCards(new CardOption[]
            {
                new CardOption { BrandCode = BrandCode.Amazon, DenomCode = DenomCode.Twenty },
                new CardOption { BrandCode = BrandCode.Amazon, DenomCode = DenomCode.FiveHundred },
                new CardOption { BrandCode = BrandCode.Amazon, DenomCode = DenomCode.Forty },
                new CardOption { BrandCode = BrandCode.Amazon, DenomCode = DenomCode.Seventy },
                new CardOption { BrandCode = BrandCode.Amazon, DenomCode = DenomCode.Sixty }
            });
            ThenHandShouldBe(HandTypes.Flush);
        }

        [Test]
        public void HandWithSameBrandSequence_ShouldBeStraightFlush()
        {
            GivenAHand().WithCards(new CardOption[]
            {
                new CardOption { BrandCode = BrandCode.Amazon, DenomCode = DenomCode.Twenty },
                new CardOption { BrandCode = BrandCode.Amazon, DenomCode = DenomCode.Thirty },
                new CardOption { BrandCode = BrandCode.Amazon, DenomCode = DenomCode.Forty },
                new CardOption { BrandCode = BrandCode.Amazon, DenomCode = DenomCode.Fifty },
                new CardOption { BrandCode = BrandCode.Amazon, DenomCode = DenomCode.Sixty }
            });
            ThenHandShouldBe(HandTypes.StraightFlush)
                .AndHandShouldNotBe(HandTypes.RoyalFlush);
        }

        [Test]
        public void HandWithSameBrandSequence_ShouldBeRoyalFlush()
        {
            GivenAHand().WithCards(new CardOption[]
            {
                new CardOption { BrandCode = BrandCode.Amazon, DenomCode = DenomCode.OneHundred },
                new CardOption { BrandCode = BrandCode.Amazon, DenomCode = DenomCode.FiveHundred },
                new CardOption { BrandCode = BrandCode.Amazon, DenomCode = DenomCode.TwoHundred },
                new CardOption { BrandCode = BrandCode.Amazon, DenomCode = DenomCode.ThreeHundred },
                new CardOption { BrandCode = BrandCode.Amazon, DenomCode = DenomCode.FourHundred }
            });
            ThenHandShouldBe(HandTypes.RoyalFlush);
        }

        private HandTests ThenHandShouldBe(HandTypes type)
        {
            _hand.Type.Should().Be(type);
            return this;
        }

        private HandTests AndHandShouldNotBe(HandTypes type)
        {
            _hand.Type.Should().NotBe(type);
            return this;
        }

        private HandTests GivenAHand()
        {
            _hand = new Hand();
            return this;
        }

        private HandTests WithNofAKindCards(int n, DenomCode denom, params BrandCode[] brands)
        {
            if (brands.Count() < n)
            {
                throw new ArgumentException(string.Format("Insufficient number of brands ({0}) specified for requested card count {1}.", brands.Count(), n));
            }

            for (var i = 0; i < n; i++)
            {
                CherryPickCard(denom, brands[i]);
            }
            return this;
        }

        private void CherryPickCard(DenomCode denom, BrandCode brand)
        {
            var selectedCard = _deck.Single(c => c.DenomCode == denom && c.BrandCode == brand);
            _hand.Add(selectedCard);
            _deck.Remove(selectedCard);
        }

        private HandTests FillWithRandomCards()
        {
            for (var i = 0; i < Hand.MaxSize - _hand.Count; i++)
            {
                var nextCard = _deck.Last();
                _hand.Add(nextCard);
                _deck.RemoveAt(_deck.Count - 1);
            }
            return this;
        }

        private HandTests WithCards(params CardOption[] options)
        {
            foreach (var cardOption in options)
            {
                CherryPickCard(cardOption.DenomCode, cardOption.BrandCode);
            }
            return this;
        }

        private class CardOption
        {
            public BrandCode BrandCode { get; set; }
            public DenomCode DenomCode { get; set; }
        }
    }
}
