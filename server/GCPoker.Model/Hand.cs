﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
namespace GCPoker.Model
{
    public class Hand : List<Card>
    {
        public const int MaxSize = 5;

        private static readonly DenomCode[] Royals = new DenomCode[]
        {
            DenomCode.OneHundred,
            DenomCode.TwoHundred,
            DenomCode.ThreeHundred,
            DenomCode.FourHundred,
            DenomCode.FiveHundred
        };

        public new void Add(Card card)
        {
            if (this.Count == MaxSize)
            {
                throw new ArgumentException(string.Format("Hand cannot exceed maximum count ({0})!", MaxSize));
            }
            base.Add(card);
        }

        public new void AddRange(IEnumerable<Card> collection)
        {
            var cardCollection = collection as Card[] ?? collection.ToArray();
            if (this.Count + cardCollection.Count() > MaxSize)
            {
                throw new ArgumentException(
                    string.Format(
                        "Hand already contains {0} cards and can contain a maximum of {1}. Cannot add more than {2}; invalid attempt to add {3}.",
                        this.Count, 
                        MaxSize, 
                        MaxSize - this.Count, 
                        cardCollection.Count()
                    )
                );
            }
            base.AddRange(cardCollection);
        }

        private bool AllSameBrand
        {
            get { return this.All(h => h.BrandCode == this.First().BrandCode); }
        }

        private bool IsASequence
        {
            get
            {
                var orderedCards = this.OrderBy(c => c.DenomCode).Select(c => c.DenomCode);
                var lastDenom = (DenomCode?)null;
                foreach (var currDenom in orderedCards)
                {
                    if (lastDenom.HasValue && ((currDenom - lastDenom) != 10))
                    {
                        return false;
                    }
                    lastDenom = currDenom;
                }
                return true;
            }
        }

        private bool IsGroupOfAKind(byte groupSize)
        {
            var groups = this.GroupBy(c => c.DenomCode);
            return groups.Any(g => g.Count() == groupSize);
        }

        private bool HasTwoGroupsOfAKind(byte group1Size, byte group2Size)
        {
            var matches = false;
            var groups = this.GroupBy(c => c.DenomCode).ToArray();

            // check for matches independently because group sizes may be the same (e.g. two pair)
            // and result in a false match if we just check for counts
            var group1SizeMatches = groups.Any(g => g.Count() == group1Size);
            if (group1SizeMatches)
            {
                var firstGroup = groups.First(g => g.Count() == group1Size);
                matches = groups.Where(g => g != firstGroup).Any(g => g.Count() == group2Size);                
            }                

            return matches;
        }

        private bool IsThreeOfAKind { get { return IsGroupOfAKind(3) && !IsFourOfAKind; } }
        private bool IsFourOfAKind { get { return IsGroupOfAKind(4); } }
        private bool IsPair { get { return IsGroupOfAKind(2) && !IsThreeOfAKind && !IsFourOfAKind; } }

        public HandTypes Type
        {
            get
            {
                if (this.All(h => Royals.Contains(h.DenomCode)) && AllSameBrand) { return HandTypes.RoyalFlush; }
                if (IsASequence && AllSameBrand) { return HandTypes.StraightFlush; }
                if (IsGroupOfAKind(4)) { return HandTypes.FourOfAKind; }
                if (HasTwoGroupsOfAKind(3, 2)) { return HandTypes.FullHouse; }
                if (!IsASequence && AllSameBrand) { return HandTypes.Flush; }
                if (IsASequence && !AllSameBrand) { return HandTypes.Straight; }
                if (IsGroupOfAKind(3) && !IsFourOfAKind) { return HandTypes.ThreeOfAKind; }
                if (HasTwoGroupsOfAKind(2, 2)) { return HandTypes.TwoPair; }
                if (IsPair && !IsThreeOfAKind && !IsFourOfAKind) { return HandTypes.OnePair; }
                return HandTypes.NotSpecial;
            }
        }

    }
}