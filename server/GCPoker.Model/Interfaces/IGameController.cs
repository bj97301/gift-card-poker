﻿using System.Collections.Generic;

namespace GCPoker.Model
{
    public interface IGameController
    {
        GameRoom GetGameRoom(int id);
        IGame GetGame();
        bool UserExists(string playerName);
        Player GetPlayerByName(string playerName);
        Player GetPlayerById(string connectionId);
        Player AddPlayer(string playerName, string connectionId);
        bool PlayerInGame(Player player);
        void UpdatePlayerId(string playerName, string connectionId);
        GameHand[] GetAllOtherPlayersHands(string exceptForConnectionId, int gameRoomId);
        List<Player> GetAllPlayers();
    }
}