﻿using System.Collections.Generic;

namespace GCPoker.Model
{
    public interface IPlayerRepository
    {
        bool HasPlayer(string playerName);
        Player Create(string name, string connectionId);
        Player GetPlayerByName(string playerName);
        Player GetPlayerById(string connectionId);
        void Update(string playerName, string connectionId);
        IDictionary<string, Player> GetAllPlayers();
    }
}