﻿namespace GCPoker.Model
{
    public interface ICardRepository
    {
        Card[] GetFullDeck();
    }
}