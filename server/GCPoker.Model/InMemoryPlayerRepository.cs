﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCPoker.Model
{
    public class InMemoryPlayerRepository : IPlayerRepository
    {
        private readonly Dictionary<string, Player> _players = new Dictionary<string, Player>();  

        public bool HasPlayer(string playerName)
        {
            return _players.ContainsKey(playerName);
        }

        public Player Create(string name, string connectionId)
        {
            var player = new Player(name, connectionId);
            _players.Add(player.Name, player);
            return player;
        }

        public Player GetPlayerByName(string playerName)
        {
            return _players[playerName];
        }

        public Player GetPlayerById(string connectionId)
        {
            return _players.SingleOrDefault(kv => kv.Value.ConnectionId == connectionId).Value;
        }

        public void Update(string playerName, string connectionId)
        {
            _players[playerName].ConnectionId = connectionId;
        }

        public IDictionary<string, Player> GetAllPlayers()
        {
            return _players;
        }
    }
}
