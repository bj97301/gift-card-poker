﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCPoker.Model
{
    public class GameController : IGameController
    {
        private readonly IPlayerRepository _playerRepository;
        private readonly IGame _game;

        private IEnumerable<Player> AllPlayers { get { return _game.GameRooms.SelectMany(g => g.Players); } }

        public GameController(IPlayerRepository playerRepository, IGame game)
        {
            _playerRepository = playerRepository;
            _game = game;
        }

        public GameRoom GetGameRoom(int id)
        {
            return _game.GetRoom(id);
        }

        public IGame GetGame()
        {
            return _game;
        }

        public bool UserExists(string playerName)
        {
            return _playerRepository.HasPlayer(playerName);
        }

        public Player AddPlayer(string name, string connectionId)
        {
            return _playerRepository.Create(name, connectionId);
        }

        public bool PlayerInGame(Player player)
        {
            return _game.GameRooms.Any(r => r.Players.Any(p => p.ConnectionId == player.ConnectionId));
        }

        public void UpdatePlayerId(string playerName, string connectionId)
        {
            _playerRepository.Update(playerName, connectionId);
        }

        public GameHand[] GetAllOtherPlayersHands(string exceptForConnectionId, int gameRoomId)
        {
            var exceptForPlayer = _playerRepository.GetPlayerById(exceptForConnectionId).Name;
            return _game.GameRooms
                .Single(gr => gr.Id == gameRoomId)
                .PlayerHands
                .Where(ph => ph.Key != exceptForPlayer)
                .Select(kv => new GameHand {PlayerName = kv.Key, Hand = kv.Value.ToArray()})
                .ToArray();
        }

        public List<Player> GetAllPlayers()
        {
            return _playerRepository.GetAllPlayers().Values.ToList();
        } 

        public Player GetPlayerByName(string name)
        {
            return _playerRepository.GetPlayerByName(name);
        }

        public Player GetPlayerById(string connectionId)
        {
            return _playerRepository.GetPlayerById(connectionId);
        }
    }
}
