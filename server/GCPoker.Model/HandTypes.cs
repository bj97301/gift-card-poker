﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCPoker.Model
{
    public enum HandTypes
    {
        NotSpecial,
        OnePair,
        TwoPair,
        ThreeOfAKind,
        Straight,
        Flush,
        FullHouse,
        FourOfAKind,        
        StraightFlush,
        RoyalFlush
    }
}
