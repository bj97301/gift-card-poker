﻿using System;
using System.Collections.Generic;

namespace GCPoker.Model
{
    public class InMemoryCardRepository : ICardRepository
    {
        public Card[] GetFullDeck()
        {
            var cards = new List<Card>();
            int currId = 1;
            foreach (var suit in Enum.GetValues(typeof(BrandCode)))
            {
                foreach (var rank in Enum.GetValues(typeof (DenomCode)))
                {
                    cards.Add(new Card()
                    {
                        Id = currId++,
                        BrandCode = (BrandCode)suit, 
                        DenomCode = (DenomCode)rank
                    });
                }
            }
            return cards.ToArray();
        }
    }
}
