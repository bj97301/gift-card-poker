﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GCPoker.Model
{
    public class Dealer
    {
        private Random random;

        public Dealer()
        {
            this.random = new Random();   
        }

        public void Change()
        {
            this.random = new Random();
        }

        /// <summary>
        /// Shuffle cards randomly. This should be good for now; for reference see
        /// http://stackoverflow.com/questions/273313/randomize-a-listt-in-c-sharp
        /// http://www.i-programmer.info/programming/theory/2744-how-not-to-shuffle-the-kunth-fisher-yates-algorithm.html
        /// (Latter provides further inspiration on reinforcing randomness)
        /// </summary>
        /// <param name="sourceCards"></param>
        /// <returns></returns>
        public IEnumerable<Card> ShuffleCards(ICollection<Card> sourceCards)
        {
            var cards = sourceCards.ToList();
            int n = cards.Count;
            while (n > 1)
            {
                n--;
                int k = this.random.Next(n + 1);
                Card card = cards[k];
                cards[k] = cards[n];
                cards[n] = card;
            }
            return cards;
        }


    }
}
