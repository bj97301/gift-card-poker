﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace GCPoker.Model
{
    public class Game : IGame
    {
        private readonly Dictionary<int, GameRoom> _gameRooms = new Dictionary<int, GameRoom>();
        private readonly ICardRepository _cardRepository;

        [JsonProperty("gameRooms")]
        public IEnumerable<GameRoom> GameRooms { get { return _gameRooms.Values; } }

        public Game(ICardRepository cardRepository)
        {
            _cardRepository = cardRepository;

            // default to two rooms to begin with
            _gameRooms.Add(1, new GameRoom(1, _cardRepository.GetFullDeck()));
            _gameRooms.Add(2, new GameRoom(2, _cardRepository.GetFullDeck()));
            _gameRooms.Add(3, new GameRoom(3, _cardRepository.GetFullDeck(), 3));
            _gameRooms.Add(4, new GameRoom(4, _cardRepository.GetFullDeck(), 4));
            _gameRooms.Add(5, new GameRoom(5, _cardRepository.GetFullDeck(), 5));
        }

        public GameRoom GetRoom(int id)
        {
            return _gameRooms[id];
        }
    }
}
