﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace GCPoker.Model
{
    public class GameRoom
    {
        [JsonProperty("id")]
        public int Id { get; set; } // not used until we support multiple game sessions

        [JsonProperty("requiredPlayerCount")]
        public int RequiredPlayerCount { get; private set; }

        [JsonProperty("gameStatus")]
        [JsonConverter(typeof(StringEnumConverter))]
        public GameStatus GameStatus { get; private set; }

        public Stack<Card> Deck { get; private set; }
        public Stack<Card> DiscardedCards { get; private set; }
        public Dictionary<string,Hand> PlayerHands { get; private set; } 
        public IEnumerable<Card> CardsInPlay { get { return this.PlayerHands.SelectMany(ph => ph.Value); } }

        [JsonProperty("players")]
        public List<Player> Players { get; private set; }


        private readonly Dealer _dealer = new Dealer();

        public GameRoom(int id, ICollection<Card> cardSupply)
        {
            Id = id;
            Deck = new Stack<Card>();
            DiscardedCards = new Stack<Card>();
            Players = new List<Player>();
            PlayerHands = new Dictionary<string, Hand>();
            RequiredPlayerCount = 2;
            GameStatus = GameStatus.NotStarted;
            InitializeCards(cardSupply);
        }

        public GameRoom(int id, ICollection<Card> cardSupply, int requiredPLayerCount) : this(id, cardSupply)
        {
            RequiredPlayerCount = requiredPLayerCount;
        }

        private void InitializeCards(ICollection<Card> cardSupply)
        {
            foreach(var card in _dealer.ShuffleCards(cardSupply))
            {
                Deck.Push(card);
            }
        }

        public Hand GetHand(string playerConnectionId)
        {
            var player = Players.SingleOrDefault(p => p.ConnectionId == playerConnectionId);
            if (player == null)
            {
                throw new ArgumentException("Specified player is not in this room!");
            }

            return PlayerHands[player.Name];
        }

        private void InitializeGame()
        {
            GameStatus = Model.GameStatus.InProgress;
            var newHands = Players.Select(p => new Hand()).ToList();

            for (var i = 0; i < Players.Count * Hand.MaxSize; i++)
            {
                var playerIndex = i%Players.Count;
                newHands[playerIndex].Add(Deck.Pop());
            }

            for (var i = 0; i < newHands.Count; i++)
            {
                PlayerHands.Add(Players[i].Name, newHands[i]);
            }
        }

        public bool JoinGame(Player player)
        {
            if (IsFull() || Players.Any(p => p.ConnectionId == player.ConnectionId))
            {
                return false;
            }

            Players.Add(player);

            if (IsFull())
            {
                InitializeGame();
            }

            return true;
        }

        public bool IsFull()
        {
            return Players.Count == RequiredPlayerCount;
        }

        public IEnumerable<Card> DiscardAndDrawNew(int[] oldCards, string connectionId)
        {
            var player = Players.Single(p => p.ConnectionId == connectionId);
            DiscardOldCards(oldCards, player);

            // Set game status ended
            if (NextPlayerAfter(connectionId) == null)
            {
                GameStatus = Model.GameStatus.Ended;
            }

            return PickNewCards(oldCards.Length, player); 
        }

        public Player NextPlayerAfter(string connectionId)
        {
            var nextPlayerIdx = Players.FindIndex(x => x.ConnectionId == connectionId) + 1;
            return (0 < nextPlayerIdx) && (nextPlayerIdx < Players.Count) ? Players[nextPlayerIdx] : null;
        }

        private IEnumerable<Card> PickNewCards(int count, Player player)
        {
            var newCards = new List<Card>();
            for (var i = 0; i < count; i++)
            {
                var card = Deck.Pop();
                newCards.Add(card);
                PlayerHands[player.Name].Add(card);
            }
            return newCards;
        }

        private void DiscardOldCards(int[] oldCards, Player player)
        {
            var matchingPlayerCards = PlayerHands[player.Name].Where(c => oldCards.Any(a => a == c.Id)).ToList();
            foreach (var card in matchingPlayerCards)
            {
                DiscardedCards.Push(card);
                PlayerHands[player.Name].Remove(card);
            }
        }
    }
}
