﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace GCPoker.Model
{
    public class GameHand
    {
        [JsonProperty("playerName")]
        public string PlayerName { get; set; }
        [JsonProperty("hand")]
        public Card[] Hand;
    }
}