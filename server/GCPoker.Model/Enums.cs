﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCPoker.Model
{
    public enum BrandCode
    {
        Amazon,
        ITunes,
        Starbucks,
        Target
    }

    public enum DenomCode
    {
        Twenty=20,
        Thirty=30,
        Forty=40,
        Fifty=50,
        Sixty=60,
        Seventy=70,
        Eighty=80,
        Ninety=90,
        OneHundred=100,
        TwoHundred=200,
        ThreeHundred=300,
        FourHundred=400,
        FiveHundred=500,
    }

    public enum GameStatus
    {
        NotStarted,
        InProgress,
        Ended
    }
}
