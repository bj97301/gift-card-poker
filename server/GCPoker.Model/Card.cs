﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace GCPoker.Model
{
    public class Card
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("brandCode")]
        [JsonConverter(typeof(StringEnumConverter))]
        public BrandCode BrandCode { get; set; }

        [JsonProperty("denomCode")]
        public DenomCode DenomCode { get; set; }
    }
}
