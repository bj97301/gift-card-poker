﻿using System;
using Newtonsoft.Json;

namespace GCPoker.Model
{
    public class Player
    {
        [JsonProperty("id")]
        public string ConnectionId { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }

        public Player(string name, string connectionId)
        {
            this.Name = name;
            this.ConnectionId = connectionId;
        }
    }
}
