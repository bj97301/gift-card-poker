using System.Collections.Generic;

namespace GCPoker.Model
{
    public interface IGame
    {
        GameRoom GetRoom(int id);
        IEnumerable<GameRoom> GameRooms { get; }
    }
}