//
//  ViewController.swift
//  gift-card-poker
//
//  Created by bj on 12/9/15.
//  Copyright © 2015 bj. All rights reserved.
//

import UIKit
import SwiftR
import SwiftyJSON

class ViewController: UIViewController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource {
    //let url="http://10.3.29.79/gameserver/signalr"
    let url="http://192.168.2.134/gameserver/signalr"
    var screenSize: CGSize!
    var screenPaddingValueWidth:CGFloat!
    var screenPaddingValueHeight:CGFloat!

    var startGameButton:UILabel!
    var sendMessageButton:UILabel!
    var chatTextField:UITextField!

    var chatScrollArea:UIScrollView!
    var chatLabelArea:UILabel!


    var gameHub:Hub!
    var chatHub:Hub!

    var playerId:String!
    var playerName:String!
    var opponentName:String!
    var yourCards:JSON!

    func showUi(){
        for view in super.view.subviews{
            view.hidden=false;
        }
    }

    override func viewDidLoad() {
        self.screenSize=UIScreen.mainScreen().bounds.size
        self.screenPaddingValueWidth=self.screenSize.width * 0.01
        self.screenPaddingValueHeight=self.screenSize.height * 0.01

        self.startGameButton=UILabel(frame: CGRect(origin: CGPoint(x: self.screenPaddingValueWidth, y: 0), size: CGSize(width: 100, height: 100)))
        self.startGameButton.text="join game"
        self.startGameButton.userInteractionEnabled=true;
        self.startGameButton.tag = 0
        self.startGameButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "tap:"))
        self.startGameButton.hidden=true;
        super.view.addSubview(self.startGameButton)


        self.sendMessageButton=UILabel(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: 200, height: 100)))
        self.sendMessageButton.text="send"
        self.sendMessageButton.userInteractionEnabled=true;
        self.sendMessageButton.tag = 1
        self.sendMessageButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "tap:"))
        self.sendMessageButton.hidden=true;
        self.sendMessageButton.sizeToFit()
        self.sendMessageButton.frame.origin.y=screenSize.height-self.sendMessageButton.frame.height-self.screenPaddingValueHeight*1.5
        self.sendMessageButton.frame.origin.x=screenSize.width-self.sendMessageButton.frame.width-self.screenPaddingValueWidth
        super.view.addSubview(self.sendMessageButton)

        self.chatTextField=UITextField()
        self.chatTextField.delegate=self
        self.chatTextField.borderStyle = UITextBorderStyle.Line
        self.chatTextField.placeholder="message"
        self.chatTextField.userInteractionEnabled=true;
        self.chatTextField.hidden=true;
        self.chatTextField.tag = 2
        self.chatTextField.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "tap:"))
        self.chatTextField.sizeToFit()
        var height=self.chatTextField.frame.height;
        var y=self.screenSize.height-self.chatTextField.frame.height-self.screenPaddingValueHeight;
        self.chatTextField.frame = CGRect(origin: CGPoint(x: self.screenPaddingValueWidth, y: y), size: CGSize(width: self.screenSize.width - self.sendMessageButton.frame.width - self.screenPaddingValueWidth*3, height: height))
        super.view.addSubview(self.chatTextField)







        self.chatScrollArea=UIScrollView()
        self.chatScrollArea.userInteractionEnabled=true;
        self.chatScrollArea.scrollEnabled=true
        self.chatScrollArea.showsVerticalScrollIndicator=true

        self.chatScrollArea.hidden=true;
        self.chatScrollArea.tag = 3

        self.chatLabelArea=UILabel()
        self.playerName="iOSPlayer#" + String(Int(arc4random_uniform(600) + 1))
        self.chatLabelArea.text="You(" + self.playerName + ") joined the chat room"

        self.chatLabelArea.userInteractionEnabled=true;
        self.chatLabelArea.tag = 4
        self.chatLabelArea.numberOfLines=1000
        self.chatLabelArea.sizeToFit()
        height=self.chatLabelArea.frame.height;
        y=200 - self.chatLabelArea.frame.height

        self.chatLabelArea.frame = CGRect(origin: CGPoint(x: 0, y: y), size: CGSize(width: self.screenSize.width - self.sendMessageButton.frame.width - self.screenPaddingValueWidth*3, height: height))

        self.chatScrollArea.addSubview(self.chatLabelArea)

        self.chatScrollArea.contentSize=self.chatLabelArea.frame.size

        self.chatScrollArea.sizeToFit()

        y=self.screenSize.height-self.chatLabelArea.frame.height-self.screenPaddingValueHeight-self.chatTextField.frame.height;

        self.chatScrollArea.frame = CGRect(origin: CGPoint(x: self.screenPaddingValueWidth, y: y-190), size: CGSize(width: self.screenSize.width - self.sendMessageButton.frame.width - self.screenPaddingValueWidth*3, height: 200))

        super.view.addSubview(self.chatScrollArea)



        //client/server stuff
        SwiftR.connect(url) { connection in
            connection.connectionSlow = { print("connectionSlow") }
            connection.reconnecting = { print("reconnecting") }
            connection.reconnected = { print("reconnected") }
            connection.disconnected = { print("disconnected") }
            connection.error = { error in
                print("Error: \(error)")

                if let source = error?["source"] as? String where source == "TimeoutException" {
                    print("Connection timed out. Restarting...")
                    connection.start()
                }
            }

            connection.connected = {
                self.playerId=connection.connectionID
                print("connected: \(connection.connectionID)")
                self.showUi()
            }
            self.chatHub = connection.createHubProxy("chatHub")

            // Event handler

            self.chatHub.on("broadcastMessage") { args in
                print("on broadcastMessage")
                let name = args!["0"] as! String
                let message = args!["1"] as! String
                let messageEntry=name+": "+message
                self.chatLabelArea.text =
                    self.chatLabelArea.text! + "\n" + messageEntry


                self.chatLabelArea.sizeToFit()
                height=self.chatLabelArea.frame.height;
                y=self.chatScrollArea.frame.height - self.chatLabelArea.frame.height
                if(y<0){
                    y=0
                }
                self.chatLabelArea.frame = CGRect(origin: CGPoint(x: 0, y: y), size: CGSize(width: self.screenSize.width - self.sendMessageButton.frame.width - self.screenPaddingValueWidth*3, height: height))
                self.chatScrollArea.contentSize=self.chatLabelArea.frame.size

                if(y<=0){
                    let bottomOffset = CGPoint(x: 0, y: self.chatScrollArea.contentSize.height - self.chatScrollArea.bounds.size.height)
                    self.chatScrollArea.setContentOffset(bottomOffset, animated: true)
                }
            }



            self.gameHub = connection.createHubProxy("gameHub")
            // Event handlers
            //            self.gameHub.on("broadcastGameMessage") { args in
            //                print("on broadcastGameMessage")
            //                let message = args!["0"] as! String
            //                let detail = args!["1"] as! String
            //                print("Message: \(message)\nDetail: \(detail)")
            //            }
            //            self.gameHub.on("gameInitialized") { args in
            //                print("on gameInitialized")
            //                //                let message = args!["0"] as! String
            //                //                let detail = args!["1"] as! String
            //                //                print("Message: \(message)\nDetail: \(detail)")
            //
            //                print("gameInitialized args:", (args!["0"])??["gameRooms"])
            //            }
            self.gameHub.on("playerJoinedGame") { args in
                print("on playerJoinedGame")
                //server calls this when a player joins
                //player, gameRoomId
                let jsonPlayer=JSON(args!)
                if(jsonPlayer["0"]["id"].stringValue != self.playerId && jsonPlayer["1"].intValue == 1){
                    print("new player")
                    self.addOpponent(jsonPlayer["0"]["name"].stringValue)
                }
                print("opponentName",self.opponentName ?? "")
                print("player json: ",JSON(args!))
            }
            self.gameHub.on("gameStarted") { args in
                var argsJson=JSON(args!)
                if(argsJson["0"]["id"].intValue==1)
                {
                    for var player in argsJson["0"]["players"].arrayValue
                    {
                        if(player["id"].stringValue != self.playerId){
                            self.addOpponent(player["name"].stringValue)
                        }
                    }
                }
                print("on gameStarted")
                print("args json",JSON(args!))
                //when you get your hand
                print("cards",args!["1"])
                let response=JSON(args!)
                self.yourCards=JSON(args!)["1"]
                self.showYourCards()
                print(response)
                print("end on gameStarted")

            }
            self.gameHub.on("beginTurn") { args in
                print("on beginTurn")
                self.reDrawLabel=UILabel(frame: CGRect(origin: CGPoint(x: self.screenPaddingValueWidth, y: 40), size: CGSize(width: 300, height: 20)))
                self.reDrawLabel.text="Discard selected cards"
                self.reDrawLabel.userInteractionEnabled=true;
                self.reDrawLabel.tag = 10
                self.reDrawLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "tap:"))
                super.view.addSubview(self.reDrawLabel)
                print("end on beginTurn")
            }
            self.gameHub.on("cardsDrawn") { args in
                //when you get new cards back

                let message = args!["0"] as! String
                let detail = args!["1"] as! String
                print("Message: \(message)\nDetail: \(detail)")

            }
        }




        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name: UIKeyboardWillHideNotification, object: nil)

    }

    var reDrawLabel:UILabel!


    func addOpponent(name:String){
        self.opponentName=self.opponentName ?? "" + " " + name
        self.showOpponents()
    }
    func keyboardWillShow(notification: NSNotification) {

        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
            self.view.frame.origin.y -= keyboardSize.height
        }

    }

    func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
            self.view.frame.origin.y += keyboardSize.height
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func tap(gesture: UIGestureRecognizer) {
        switch gesture.view!.tag{
        case 0://start game
            startGame()
        case 1://send message
            sendChatMessage(self.chatTextField.text!,name:playerName, hub: self.chatHub)
            self.chatTextField.resignFirstResponder()
        default:
            return;
        }


    }

    func textFieldShouldReturn(textField: UITextField) -> Bool {

        //textField code
        textField.becomeFirstResponder()
        sendChatMessage(self.chatTextField.text!,name:playerName, hub: self.chatHub)
        return true
    }


    var opponentsLabel:UILabel!

    var cardTableView:UITableView!

    func showYourCards(){
        cardTableView=UITableView(frame: CGRect(origin: CGPoint(x: 0, y: 70), size: CGSize(width: self.screenSize.width, height: 220)))
        cardTableView.delegate=self
        cardTableView.dataSource=self
        super.view.addSubview(cardTableView)
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.yourCards.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        let cell:UITableViewCell = UITableViewCell()

        print("your cards",self.yourCards)

        let text=self.yourCards[indexPath.row]["BrandCode"].stringValue + " $" + self.yourCards[indexPath.row]["DenomCode"].stringValue

        cell.textLabel?.text = text
        return cell

    }


    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = cardTableView.cellForRowAtIndexPath(indexPath)
        if(cell?.accessoryType == UITableViewCellAccessoryType.Checkmark){
            cell?.accessoryType=UITableViewCellAccessoryType.None
        }
        else
        {
            cell?.accessoryType=UITableViewCellAccessoryType.Checkmark
        }
        self.cardTableView.deselectRowAtIndexPath(indexPath, animated: true)
        print("You selected cell #\(indexPath.row)!")
    }


    func showOpponents(){
        self.opponentsLabel=UILabel()
        self.opponentsLabel.text="Opponents: " + self.opponentName ?? "";
        self.opponentsLabel.sizeToFit()
//        frame: CGRect(origin: CGPoint(x: self.screenPaddingValueWidth, y: 0), size: CGSize(width: 100, height: 100))
        self.opponentsLabel.frame.origin=CGPoint(x: self.screenPaddingValueWidth, y: 20)
        self.opponentsLabel.userInteractionEnabled=false;

        super.view.addSubview(self.opponentsLabel)

    }





    func startGame() {
        print("start game clicked")
        self.gameHub.invoke("initializeClientGame", arguments: [playerName]) { (result, error) in
            if let e = error {
                print("Error message: \(e)")

            } else {
                print("joinGame Success!")
                if let r = result {
                    print("Result: \(r)")

                }
            }
        }

        self.gameHub.invoke("joinGame", arguments: [1]) { (result, error) in

            if let e = error {
                print("Error message: \(e)")

            } else {
                print("joinGame Success!")
                self.startGameButton.hidden=true
            }
        }
    }
    func sendChatMessage(message: String, name: String, hub: Hub){
        hub.invoke("send", arguments: [name, message]) { (result, error) in
            print("invoke")
            if let e = error {
                print("Error message: \(e)")
            } else {
                print("sent message")
                self.chatTextField.text=""
                //self.chatTextField.resignFirstResponder()
                if let r = result {
                    print("Result: \(r)")
                }
            }
        }
    }
}
